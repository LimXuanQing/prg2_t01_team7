﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class Resident: Person
    {
        //property
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }

        //constructor
        public Resident(string name,string address, DateTime lastLeftCountry):base(name)
        {
            Address = address;
            LastLeftCountry = lastLeftCountry;
        }

        //function to calculate SHN charge
        public override double CalculateSHNCharges()
        {
            foreach(TravelEntry t in TravelEntryList)
            {
                if (t.LastCountryOfEmbarkation== "New Zealand" || t.LastCountryOfEmbarkation == "Vietnam")
                {
                    double sdf = 0.00;
                    double transportation = 0.00;
                    double swab = 200.00 * 1.07;
                    return sdf + transportation + swab;

                }
                else if (t.LastCountryOfEmbarkation == "Macao SAR")
                {
                    double sdf = 0.00;
                    double transportation = 20.00 * 1.07;
                    double swab = 200.00 * 1.07;
                    return sdf + transportation + swab;
                }
                else
                {
                    double sdf = 1000.00 * 1.07;
                    double transportation = 20.00 * 1.07;
                    double swab = 200.00 * 1.07;
                    return sdf + transportation + swab;
                }
            }
            return 0.00;

        }

        public override string ToString()
        {
            return base.ToString() + "Address: " + Address + "\tLast Left Country: " + LastLeftCountry +"\nTraceTogether Token: "+Token;
        }
    }
}
