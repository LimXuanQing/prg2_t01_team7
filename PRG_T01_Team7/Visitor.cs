﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class Visitor : Person
    {
        //Properties

        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        // Constructors

        public Visitor(string name, string pn, string nationality) : base(name)
        {
            PassportNo = pn;
            Nationality = nationality;
        }

        // Methods

        /// <summary>
        /// Calculate the SHN Charges based on last country of embarkation, charges includes: SHN facility, transportaion and swab test
        /// </summary>
        /// <returns></returns>
        
        // CalculateSHNCharges
        public override double CalculateSHNCharges()
        {
            foreach (TravelEntry t in TravelEntryList)
            {
                if (t.LastCountryOfEmbarkation == "New Zealand" || t.LastCountryOfEmbarkation == "Vietnam")
                {
                    double sdf = 0.00;
                    double transportation = 80.00;
                    double swab = 200.00 * 1.07;
                    double cost = sdf + transportation + swab;
                    return cost;

                }
                else if (t.LastCountryOfEmbarkation == "Macao SAR")
                {
                    double sdf = 0.00;
                    double transportation = 80.00 * 1.07;
                    double swab = 200.00 * 1.07;
                    double cost = sdf + transportation + swab;
                    return cost;
                }
                else
                {
                    double sdf = 2000.00 * 1.07;
                    double transportation = t.SHNstay.CalculateTravelCost(t.EntryMode, t.EntryDate) * 1.07;
                    double swab = 200.00 * 1.07;
                    double cost = sdf + transportation + swab;
                    return cost;
                }
            }
            return 0.00;
        }

        public override string ToString()
        {
            return base.ToString() + "\tPassport No: " + PassportNo + "\tNationality: " + Nationality;
        }

    }

}
