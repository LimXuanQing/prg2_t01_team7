﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Linq;

namespace PRG_T01_Team7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();   //list to store person
            List<BusinessLocation> businessList = new List<BusinessLocation>(); //list to store business
            List<Person> tracingList = new List<Person>();  //list to store tracing report person
            List<Person> shnstatusList = new List<Person>();    //list to store shn status report person

            InitPerson(personList);  //call InitPerson()
            InitBusiness(businessList); //call InitBusiness()

            while (true)    
            {
                DisplayMenu();  //call DisplayMenu()
                Console.Write("Enter your option: ");   //prompt user to enter option for menu
                string option = Console.ReadLine();
                if (option == "1")
                {
                    PersonSearch(personList);   //Person Details
                }
                else if (option == "2")
                {
                    ReplaceTraceTogetherToken(personList);  //Assign or Replace Trace Together Token
                }
                else if (option == "3")
                {
                    ListBusiness2(businessList);    //Business Location
                }
                else if (option == "4")
                {
                    EditBusinessLocationCapacity(businessList); //Edit Business Location Capacity
                }
                else if (option == "5")
                {
                    CreateSafeEntry(personList, businessList);  //SafeEntry Check-In
                }
                else if (option == "6")
                {
                    CheckOut(personList);   //SafeEntry Check-Out
                }
                else if (option == "7")
                {
                    // Load SHN Facility Data - call API and populate a list
                    InitSHNFacilityData();
                }
                else if (option == "8")
                {
                    // List all Visitors
                    DisplayVisitor(personList);
                }
                else if (option == "9")
                {
                    // Create Visitor
                    CreateVisitor(personList);
                }
                else if (option == "10")
                {
                    // Create TravelEntry record
                    TravelEntryRecord(personList);

                }

                else if (option == "11")
                {
                    // Calculate SHN Charges
                    CalculateSHNCharges(personList);
                }
                else if (option == "12")
                {
                    TraceReport(personList, tracingList);   //Generate Contact Tracing Report
                }
                else if (option == "13")
                {
                    StatusReport(personList, shnstatusList);    //Generate SHN Status Reporting
                }
                else if (option == "0")
                {
                    Console.WriteLine("Terminating program... GoodBye");    //exit program
                    break;
                }
                else
                    Console.WriteLine("Invalid option. Please Try Again."); //validation
            }


        }

        //method to display main menu
        static void DisplayMenu()
        {
            Console.WriteLine("\n--------COVID-19 Monitoring System--------" +
                           "\n[1] Person Details" +
                           "\n[2] Assign or Replace Trace Together Token" +
                           "\n[3] Business Location" +
                           "\n[4] Edit Business Location Capacity" +
                           "\n[5] SafeEntry Check-In" +
                           "\n[6] SafeEntry Check-Out" +
                           "\n[7] SHN Facility" +
                           "\n[8] Visitor List" +
                           "\n[9] Create Visitor" +
                           "\n[10] TravelEntry Record" +
                           "\n[11] Calculate SHN Charges" +
                           "\n[12] Generate Contact Tracing Report" +
                           "\n[13] Generate SHN Status Reporting" +
                           "\n[0] Exit" +
                           "\n------------------------------------------");

        }

        //method to initperson from csv
        static void InitPerson(List<Person> pList)
        {
            string[] data = File.ReadAllLines("Person.csv");    //read file
            for (int i = 1; i < data.Length; i++)
            {
                string[] lines = data[i].Split(",");
                string type = lines[0];
                if (type == "resident") //resident
                {
                    if (!string.IsNullOrWhiteSpace(lines[6]) && string.IsNullOrWhiteSpace(lines[9]))    //resident with token
                    {
                        string name = lines[1];
                        string address = lines[2];
                        DateTime lastLeftCountry = Convert.ToDateTime(lines[3]);
                        Resident r = new Resident(name, address, lastLeftCountry);  //add new resident

                        string serialNo = lines[6];
                        string collectionLocation = lines[7];
                        DateTime expiryDate = Convert.ToDateTime(lines[8]);

                        r.Token = new TraceTogetherToken(serialNo, collectionLocation, expiryDate); //add new token
                        pList.Add(r);
                    }
                    else if (string.IsNullOrWhiteSpace(lines[6]) && !string.IsNullOrWhiteSpace(lines[9]))   //resident with travel entry
                    {
                        string name = lines[1];
                        string address = lines[2];
                        DateTime lastLeftCountry = Convert.ToDateTime(lines[3]);
                        Resident r = new Resident(name, address, lastLeftCountry);  //add new resident

                        string country = lines[9];
                        string mode = lines[10];
                        DateTime entryDate = Convert.ToDateTime(lines[11]);
                        DateTime shnEndDate = Convert.ToDateTime(lines[12]);
                        string shnName = lines[14];
                        SHNFacility s = SearchSHNFacility(InitSHNFacilityList(), shnName);
                        bool isPaid = Convert.ToBoolean(lines[13]);
                        TravelEntry te = new TravelEntry(country, mode, entryDate); //add new travelEntry
                        te.SHNEndDate = shnEndDate;
                        te.SHNstay = s;
                        te.Ispaid = isPaid;

                        r.AddTravelEntry(te);
                        pList.Add(r);
                    }
                    else if (!string.IsNullOrWhiteSpace(lines[6]) && !string.IsNullOrWhiteSpace(lines[9]))  //resident with both
                    {
                        string name = lines[1];
                        string address = lines[2];
                        DateTime lastLeftCountry = Convert.ToDateTime(lines[3]);
                        Resident r = new Resident(name, address, lastLeftCountry);  //add new resident

                        string serialNo = lines[6];
                        string collectionLocation = lines[7];
                        DateTime expiryDate = Convert.ToDateTime(lines[8]);

                        r.Token = new TraceTogetherToken(serialNo, collectionLocation, expiryDate); //add new token

                        string country = lines[9];
                        string mode = lines[10];
                        DateTime entryDate = Convert.ToDateTime(lines[11]);
                        DateTime shnEndDate = Convert.ToDateTime(lines[12]);
                        string shnName = lines[14];
                        SHNFacility s = SearchSHNFacility(InitSHNFacilityList(), shnName);
                        bool isPaid = Convert.ToBoolean(lines[13]);
                        TravelEntry te = new TravelEntry(country, mode, entryDate); //add new travelEntry
                        te.SHNEndDate = shnEndDate;
                        te.SHNstay = s;
                        te.Ispaid = isPaid;

                        r.AddTravelEntry(te);
                        pList.Add(r);
                    }
                    //resident with nothing
                    else
                    {
                        string name = lines[1];
                        string address = lines[2];
                        DateTime lastLeftCountry = Convert.ToDateTime(lines[3]);
                        Resident r = new Resident(name, address, lastLeftCountry);  //add new resident
                        pList.Add(r);
                    }
                }
                else if (type == "visitor") //visitor
                {
                    if (!string.IsNullOrWhiteSpace(lines[9]))   //visitor with travelEntry
                    {
                        string name = lines[1];
                        string passportNo = lines[4];
                        string nationality = lines[5];
                        Visitor v = new Visitor(name, passportNo, nationality); //add new visitor

                        string country = lines[9];
                        string mode = lines[10];
                        DateTime entryDate = Convert.ToDateTime(lines[11]);
                        DateTime shnEndDate = Convert.ToDateTime(lines[12]);
                        string shnName = lines[14];
                        SHNFacility s = SearchSHNFacility(InitSHNFacilityList(), shnName);
                        bool isPaid = Convert.ToBoolean(lines[13]);
                        TravelEntry te = new TravelEntry(country, mode, entryDate); //add new travel entry
                        te.SHNEndDate = shnEndDate;
                        te.SHNstay = s;
                        te.Ispaid = isPaid;

                        v.AddTravelEntry(te);
                        pList.Add(v);
                    }
                    //visitor with nothing
                    else
                    {
                        string name = lines[1];
                        string passportNo = lines[4];
                        string nationality = lines[5];
                        Visitor v = new Visitor(name, passportNo, nationality); //add new visitor
                        pList.Add(v);
                    }

                }
            }
        }

        //method to init business from csv
        static void InitBusiness(List<BusinessLocation> bList)
        {
            string[] data = File.ReadAllLines("BusinessLocation.csv");  //read file
            for (int i = 1; i < data.Length; i++)
            {
                string[] lines = data[i].Split(",");

                string businessName = lines[0];
                string branchCode = lines[1];
                int maximumCapacity = Convert.ToInt32(lines[2]);
                bList.Add(new BusinessLocation(businessName, branchCode, maximumCapacity)); //add new business

            }
        }

        //method to search person
        static Person SearchPerson(List<Person> pList, string name)
        {
            foreach (Person p in pList)
            {
                if (p is Resident)
                {
                    Resident r = (Resident)p;   //downcast
                    if (r.Name.ToLower() == name.ToLower())
                    {
                        return r;   //return resident
                    }
                }

                if (p is Visitor)
                {
                    Visitor v = (Visitor)p; //downcast
                    if (v.Name.ToLower() == name.ToLower())
                    {
                        return v;   //return visitor
                    }
                }
            }
            return null;    //return null if not found
        }

        //method to search and display person details (option 1)
        static void PersonSearch(List<Person> pList)
        {
            while (true)
            {
                Console.Write("Enter person name: ");   //prompt user for person name
                string name = Console.ReadLine();
                Person p = SearchPerson(pList, name);   //search for the person
                if (p != null)  //found
                {
                    PersonDetail(p);    //details
                    break;
                }
                else
                {
                    Console.WriteLine("Person does not exist!");    //validation
                }
            }
        }

        //method to list business location (option3)
        static void ListBusiness2(List<BusinessLocation> bList)
        {
            int i = 1;
            foreach (BusinessLocation b in bList)
            {
                Console.WriteLine("\n[{0}]" +
                            "\n-----------------------------------------" +
                           "\n|\t\t\t\t\t|" +
                           "\n|\t-------------------------\t|" +
                           "\n|\t|{1}\t|\t|" +
                           "\n|\t-------------------------\t|" +
                           "\n|\t\t---------\t\t|" +
                           "\n|\t\t|\t|\t\t|" +
                           "\n|\t\t|O\t|\t\t|" +
                           "\n|\t\t|\t|\t\t|" +
                           "\n-----------------------------------------" +
                           "\nBranch Code: {2}" + "\nMaximum Capacity: {3}" + "\nVisitors Now: {4}", i, b.BusinessName, b.BranchCode, b.MaximumCapacity, b.VisitorsNow);
                i++;
            }
        }

        //method to list person details
        static void PersonDetail(Person p)
        {

            if (p is Resident)
            {
                Resident r = (Resident)p;
                Console.WriteLine("\n{0, -30} {1, -30} {2, -0}", "Name", "Address", "Last Left Country");
                Console.WriteLine("{0, -30} {1, -30} {2, -30}", r.Name, r.Address, r.LastLeftCountry);
                //string seList = "";
                //string teList = "";

                Console.WriteLine("\nTravel Entry List: ");
                Console.WriteLine("{0, -30} {1, -30} {2, -30} {3, -30} {4, -30}", "Last Country Of Embarkation", "Entry Mode", "Entry Date", "SHN End Date", "Is Paid");
                foreach (TravelEntry te in r.TravelEntryList)
                {
                    Console.WriteLine("{0, -30} {1, -30} {2, -30} {3, -30} {4, -30}", te.LastCountryOfEmbarkation, te.EntryMode, te.EntryDate, te.SHNEndDate, te.Ispaid);
                    Console.WriteLine("\nSHN Stay: ");
                    Console.WriteLine(te.SHNstay);
                }

                Console.WriteLine("\nSafeEntry List: ");
                Console.WriteLine("{0, -30} {1, -30} {2, -30}", "Check In", "Check Out", "Location");
                foreach (SafeEntry se in r.SafeEntryList)
                {
                    Console.WriteLine("{0, -30} {1, -30} {2, -30}", se.CheckIn, se.CheckOut, se.Location);
                }
                if (r.Token != null)
                {
                    Console.WriteLine("\nTrace Together Token: ");
                    Console.WriteLine("{0, -30} {1, -30} {2, -30}", "Serial No", "Collection Location", "Expiry Date");
                    Console.WriteLine("{0, -30} {1, -30} {2, -30}", r.Token.SerialNo, r.Token.CollectionLocation, r.Token.ExpiryDate);
                }
            }
            else if (p is Visitor)
            {
                Visitor v = (Visitor)p;
                Console.WriteLine("\n{0, -30} {1, -30} {2, -30}", "Name", "Passport No", "Nationality");
                Console.WriteLine("{0, -30} {1, -30} {2, -30}", v.Name, v.PassportNo, v.Nationality);
                //string seList = "";
                //string teList = "";
                Console.WriteLine("\nTravel Entry List: ");
                Console.WriteLine("{0, -30} {1, -30} {2, -30} {3, -30} {4, -30}", "Last Country Of Embarkation", "Entry Mode", "Entry Date", "SHN End Date", "Is Paid");
                foreach (TravelEntry te in v.TravelEntryList)
                {
                    Console.WriteLine("{0, -30} {1, -30} {2, -30} {3, -30} {4, -30}", te.LastCountryOfEmbarkation, te.EntryMode, te.EntryDate, te.SHNEndDate, te.Ispaid);
                    Console.WriteLine("\nSHN Stay: ");
                    Console.WriteLine(te.SHNstay);
                }

                Console.WriteLine("\nSafeEntry List: ");
                Console.WriteLine("{0, -30} {1, -30} {2, -30}", "Check In", "Check Out", "Location");
                foreach (SafeEntry se in v.SafeEntryList)
                {
                    Console.WriteLine("{0, -30} {1, -30} {2, -30}", se.CheckIn, se.CheckOut, se.Location);
                }
            }
        }

        //method to replace token (option 2)
        static void ReplaceTraceTogetherToken(List<Person> pList)
        {
            Console.Write("Please Enter Your Name: ");  //prompt user for name
            string name = Console.ReadLine();
            Person p = SearchPerson(pList, name);   //search person

            if (p is Resident)
            {
                Resident r = (Resident)p;   //downcast
                if (r.Name.ToLower() == name.ToLower())
                {
                    if (r.Token != null)
                    {
                        if (r.Token.IsEligibleForReplacement())//check if token is eligible for replacement
                        {
                            Random ran = new Random();
                            var x = ran.Next(0, 100000);
                            string s = "T" + x.ToString("00000");   //generate a random serial number
                            Console.Write("Please Enter the Nearest Community Center: ");   //prompt user for collection location
                            string collectionLocation = Console.ReadLine();
                            r.Token.ReplaceToken(s, collectionLocation);    //call replaceToken method
                        }
                        else
                        {
                            Console.WriteLine("Token can be replaced only within 1 month from its expiry"); //validation
                        }
                    }
                    else if (r.Token == null)   //resident who does not have a token
                    {
                        Random ran = new Random();
                        var x = ran.Next(0, 100000);
                        string s = "T" + x.ToString("00000");   //generate a random serial no
                        string serialNo = s;
                        Console.Write("Please Enter the Nearest Community Center: ");   //prompt user for collection location
                        string collectionLocation = Console.ReadLine();
                        DateTime expiryDat = DateTime.Today.AddMonths(6);
                        r.Token = new TraceTogetherToken(serialNo, collectionLocation, expiryDat);  //add new token 
                        Console.WriteLine("Please collect your token at the community center!");
                    }
                }
                else
                {
                    Console.WriteLine("Person is not a resident."); //validation
                }
            }
            else
            {
                Console.WriteLine("Person is not a resident."); //validation
            }
        }

        //method to list safe entry that resident have not check out
        static void ListSafeEntry(Person p)
        {
            DateTime aDate = new DateTime(0001, 1, 1);
            for (int i = 0; i < p.SafeEntryList.Count; i++)
            {
                if (p.SafeEntryList[i].CheckOut == aDate)
                {
                    Console.WriteLine("[{0}] {1}", i + 1, p.SafeEntryList[i]);
                }

            }
        }

        //method to edit business location's capacity (option4)
        static void EditBusinessLocationCapacity(List<BusinessLocation> bList)
        {
            while (true)
            {
                try
                {
                    Console.Write("Enter Business Code: "); //prompt user for branch code
                    string businessCode = Console.ReadLine();
                    Console.Write("Enter Maximum Capacity: ");  //prompt user for new capacity
                    int capacity = Convert.ToInt32(Console.ReadLine());
                    BusinessLocation b = SearchBusiness(bList, businessCode);   //search business
                    if (b != null)
                    {
                        b.MaximumCapacity = capacity;   //edit capacity
                        Console.WriteLine("Capacity updated!");
                    }
                    else
                    {
                        Console.WriteLine("Business location does not exist."); //validation
                    }
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }

        //method to search business
        static BusinessLocation SearchBusiness(List<BusinessLocation> bList, string businesscode)
        {
            foreach (BusinessLocation b in bList)
            {
                if (b.BranchCode == businesscode)
                {
                    return b;   //return business
                }
            }
            return null;    //return null if not found
        }

        //method to create safe entry (option5)
        static void CreateSafeEntry(List<Person> pList, List<BusinessLocation> bList)
        {
            while (true)
            {
                try
                {
                    Console.Write("Enter your name: "); //prompt user for name
                    string name = Console.ReadLine();
                    bool found = false;
                    Person p = SearchPerson(pList, name);   //search person
                    if (p!= null)
                    {
                        found = true;
                    }
                    else
                    {
                        found = false;
                    }
                    ListBusiness2(bList);                   //list business location
                    Console.Write("Enter your option: ");   //prompt user to select business
                    int seOption = Convert.ToInt32(Console.ReadLine());
                    BusinessLocation b = bList[seOption - 1];
                    if (b.IsFull()) //check business is full
                    {
                        Console.WriteLine("Business Location is full.");
                    }
                    else
                    {
                        if (found)
                        {
                            p.AddSafeEntry(new SafeEntry(DateTime.Now, b)); //add sefe entry to person if not full
                            b.VisitorsNow += 1;                             //visitors now for business increase by 1
                            Console.WriteLine("Welcome to {0}!", b.BusinessName);
                        }
                        else
                        {
                            Console.WriteLine("Person not found."); //validation
                        }
                        
                    }
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }

        //method to check out (option6)
        static void CheckOut(List<Person> pList)
        {
            while (true)
            {
                try
                {
                    Console.Write("Enter your name: "); //prompt user for name
                    string name = Console.ReadLine();
                    bool found = false;
                    Person p = SearchPerson(pList, name);   //search person
                    if (p != null)
                    {
                        found = true;
                    }
                    else
                    {
                        found = false;
                    }
                    if (found)
                    {
                        ListSafeEntry(p);                       //list safe entry that have not check out
                        Console.Write("Enter your option: ");   //prompt user to select which to check out
                        int coOption = Convert.ToInt32(Console.ReadLine());
                        SafeEntry se = p.SafeEntryList[coOption - 1];
                        se.PerformCheckOut();                   //perform check out
                        DecreaseVisitorsNow(se.Location);       //visitors now for business decrease by 1
                        Console.WriteLine("Check Out Sucessfully. See You Next Time!");
                    }
                    else
                    {
                        Console.WriteLine("Person not found."); //validation
                    }
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }

        //method to decrease visitors now
        static void DecreaseVisitorsNow(BusinessLocation b)
        {
            b.VisitorsNow -= 1; //visitors now decrease by 1
        }

        /// <summary>
        /// option 7
        /// Load SHN Facility Data - call API and populate a list
        /// </summary>
        static void InitSHNFacilityData()
        {
            using (HttpClient client = new HttpClient())
            {

                List<SHNFacility> SHNFacilityList = new List<SHNFacility>();
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    SHNFacilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);

                    //List all SHN Facilities
                    Console.WriteLine("{0,-25}  {1,-25}  {2,-25}  {3,-25}  {4,-25}", "Facility Name", "Facility Capacity", "DistFromAirCheckpoint", "DistFromSeaCheckpoint", "DistFromLandCheckpoint");

                    foreach (SHNFacility shn in SHNFacilityList)
                    {
                        Console.WriteLine("{0,-25}  {1,-25}  {2,-25}  {3,-25}  {4,-25}",
                                           shn.FacilityName, shn.FacilityCapacity, shn.DistFromAirCheckpoint, shn.DistFromSeaCheckpoint, shn.DistFromLandCheckpoint);

                    }
                }
            }
        }

        /// <summary>
        /// List of SHN Facilities to be used in other methods to get SHN details
        /// </summary>
        /// <returns></returns>
        static List<SHNFacility> InitSHNFacilityList()
        {
            using (HttpClient client = new HttpClient())
            {
                List<SHNFacility> SHNFacilityList = new List<SHNFacility>();
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    SHNFacilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);

                    foreach (SHNFacility shn in SHNFacilityList)
                    {
                        shn.FacilityVacancy = shn.FacilityCapacity;
                    }
                }
                //Console.WriteLine(SHNFacilityList);
                return SHNFacilityList;
            }
        }

        /// <summary>
        /// option 8
        /// List all Visitors
        /// Pass back to person list 
        /// </summary>
        /// <param name="pList"></param>
        static void DisplayVisitor(List<Person> pList)
        {
            Console.WriteLine("{0,-20} {1,-20} {2,-20}", "Name", "Passport No", "Nationality");
            foreach (Person p in pList)
            {
                if (p is Visitor)
                {
                    Visitor v = (Visitor)p;
                    Console.WriteLine("{0,-20} {1,-20} {2,-20}", v.Name, v.PassportNo, v.Nationality);
                }
            }
        }

        /// <summary>
        /// option 9
        /// Create Visitors
        /// 1. Prompt user for details
        /// 2. Create Visitor object
        /// Pass back to person list 
        /// </summary>
        /// <param name="pList"></param>
        static void CreateVisitor(List<Person> pList)
        {
            // prompt user for details
            Console.Write("Visitor Name: ");
            string name = Console.ReadLine();
            Console.Write("Visitor PassportNo: ");
            string passportNo = Console.ReadLine();
            Console.Write("Visitor Nationality: ");
            string nationality = Console.ReadLine();

            // create Visitor object
            pList.Add(new Visitor(name, passportNo, nationality));

        }

        /// <summary>
        /// option 10
        /// Create TravelEntry Record
        /// 1. Prompt user for details
        /// 2. Search for Person
        /// 3. Prompt user for details (last country of embarkation, entry mode)
        /// </summary>
        /// <param name="pList"></param>
        static void TravelEntryRecord(List<Person> pList)
        {
            while (true)
            {
                try
                {
                    // prompt user for name
                    Console.Write("Please enter your name: ");
                    string name = Console.ReadLine();

                    // search for the person
                    Person p = SearchPerson(pList, name);
                    if (pList.Contains(p) == true)
                    {
                        // prompt user for details (last country of embarkation, entry mode)
                        Console.Write("Please enter your last country of embarkation: ");
                        string lcoe = Console.ReadLine();
                        Console.Write("Please enter your entry mode: ");
                        string entryMode = Console.ReadLine();
                        Console.Write("Please enter your entry date (yyyy/mm/dd): ");
                        DateTime entryDate = Convert.ToDateTime(Console.ReadLine());

                        if (lcoe == "New Zealand" || lcoe == "Vietnam")
                        {
                            TravelEntry te = new TravelEntry(lcoe, entryMode, entryDate);
                            te.CalculateSHNDuration();
                            // call AddTravelEntry() in Person to assign the TravelEntry object
                            p.AddTravelEntry(te);
                        }
                        else
                        {
                            // create TravelEntry object
                            TravelEntry te = new TravelEntry(lcoe, entryMode, entryDate);
                            // call AddTravelEntry() in Person to assign the TravelEntry object
                            p.AddTravelEntry(te);

                            // call CalculateSHNDuration() to calculate SHNEndDate based on criteria given in the background brief
                            te.CalculateSHNDuration();

                            // list SHN facilities if necessary, for user to select
                            Console.WriteLine("List of SHN Facilities: ");
                            FacilityVacancy(InitSHNFacilityList());

                            // assign chosen SHN facility if necessary, and reduce the vacancy count
                            Console.Write("Please enter the name of the SHN Facility that you have chosen: ");
                            string shnfacilityName = Console.ReadLine();

                            p.TravelEntryList[0].SHNstay = SearchSHNFacility(InitSHNFacilityList(), shnfacilityName);

                            if (p.TravelEntryList[0].SHNstay.FacilityVacancy == 0)
                            {
                                Console.WriteLine("{0} SHN Facility is not available.", shnfacilityName);
                            }
                            else
                            {
                                p.TravelEntryList[0].SHNstay.FacilityVacancy -= 1;
                                Console.WriteLine(p.TravelEntryList[0].SHNstay);
                                Console.WriteLine("You are assigned to {0} SHN Facility.", shnfacilityName);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("This person does not exist.");   //validation
                    }
                    break;
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }

        /// <summary>
        /// option 10
        /// Calculate Facility Vacancy
        /// SHNFacility List with Facility Vacancy
        /// </summary>
        /// <param name="sList"></param>
        static void FacilityVacancy(List<SHNFacility> sList)
        {
            int people1 = 0;
            int people2 = 0;
            int people3 = 0;
            int people4 = 0;
            string[] data = File.ReadAllLines("Person.csv");
            for (int i = 1; i < data.Length; i++)
            {
                string[] lines = data[i].Split(",");
                string facilityName = lines[14];
                if (!string.IsNullOrWhiteSpace(lines[14]))
                {
                    if (facilityName == "A'Resort")
                    {
                        foreach (SHNFacility s in sList)
                        {
                            if (facilityName == s.FacilityName)
                            {
                                bool success = s.IsAvailable(people1);
                                if (success == true)
                                {
                                    people1 += 1;
                                    s.FacilityVacancy = s.FacilityCapacity - people1;
                                }
                                else
                                {
                                    s.FacilityVacancy = s.FacilityCapacity;
                                }
                            }
                        }

                    }
                    else if (facilityName == "Yozel")
                    {
                        foreach (SHNFacility s in sList)
                        {
                            if (facilityName == s.FacilityName)
                            {
                                bool success = s.IsAvailable(people2);
                                if (success == true)
                                {
                                    people2 += 1;
                                    s.FacilityVacancy = s.FacilityCapacity - people2;
                                }
                                else
                                {
                                    s.FacilityVacancy = s.FacilityCapacity;
                                }
                            }
                        }
                    }
                    else if (facilityName == "Mandarin Orchid")
                    {
                        foreach (SHNFacility s in sList)
                        {
                            if (facilityName == s.FacilityName)
                            {
                                bool success = s.IsAvailable(people3);
                                if (success == true)
                                {
                                    people3 += 1;
                                    s.FacilityVacancy = s.FacilityCapacity - people3;
                                }
                                else
                                {
                                    s.FacilityVacancy = s.FacilityCapacity;
                                }
                            }
                        }
                    }
                    else if (facilityName == "Small Hostel")
                    {
                        foreach (SHNFacility s in sList)
                        {
                            if (facilityName == s.FacilityName)
                            {
                                bool success = s.IsAvailable(people4);
                                if (success == true)
                                {
                                    people4 += 1;
                                    s.FacilityVacancy = s.FacilityCapacity - people4;
                                }
                                else
                                {
                                    s.FacilityVacancy = s.FacilityCapacity;
                                }
                            }
                        }
                    }
                }
            }
            foreach (SHNFacility shn in sList)
            {
                Console.WriteLine(shn);
            }
        }

        /// <summary>
        /// option 10
        /// assign chosen SHN facility if necessary, and reduce the vacancy count
        /// </summary>
        /// <param name="sList"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        static SHNFacility SearchSHNFacility(List<SHNFacility> sList, string name)
        {
            foreach (SHNFacility s in sList)
            {
                if (s.FacilityName == name)
                {
                    int people1 = 0;
                    int people2 = 0;
                    int people3 = 0;
                    int people4 = 0;
                    string[] data = File.ReadAllLines("Person.csv");
                    for (int i = 1; i < data.Length; i++)
                    {
                        string[] lines = data[i].Split(",");
                        string facilityName = lines[14];
                        if (!string.IsNullOrWhiteSpace(lines[14]))
                        {
                            if (facilityName == "A'Resort")
                            {
                                foreach (SHNFacility ss in sList)
                                {
                                    if (facilityName == ss.FacilityName)
                                    {
                                        bool success = ss.IsAvailable(people1);
                                        if (success == true)
                                        {
                                            people1 += 1;
                                            ss.FacilityVacancy = ss.FacilityCapacity - people1;
                                        }
                                        else
                                        {
                                            ss.FacilityVacancy = ss.FacilityCapacity;
                                        }
                                    }
                                }

                            }
                            else if (facilityName == "Yozel")
                            {
                                foreach (SHNFacility ss in sList)
                                {
                                    if (facilityName == ss.FacilityName)
                                    {
                                        bool success = ss.IsAvailable(people2);
                                        if (success == true)
                                        {
                                            people2 += 1;
                                            ss.FacilityVacancy = ss.FacilityCapacity - people2;
                                        }
                                        else
                                        {
                                            ss.FacilityVacancy = ss.FacilityCapacity;
                                        }
                                    }
                                }
                            }
                            else if (facilityName == "Mandarin Orchid")
                            {
                                foreach (SHNFacility ss in sList)
                                {
                                    if (facilityName == ss.FacilityName)
                                    {
                                        bool success = ss.IsAvailable(people3);
                                        if (success == true)
                                        {
                                            people3 += 1;
                                            ss.FacilityVacancy = ss.FacilityCapacity - people3;
                                        }
                                        else
                                        {
                                            ss.FacilityVacancy = ss.FacilityCapacity;
                                        }
                                    }
                                }
                            }
                            else if (facilityName == "Small Hostel")
                            {
                                foreach (SHNFacility ss in sList)
                                {
                                    if (facilityName == ss.FacilityName)
                                    {
                                        bool success = ss.IsAvailable(people4);
                                        if (success == true)
                                        {
                                            people4 += 1;
                                            ss.FacilityVacancy = ss.FacilityCapacity - people4;
                                        }
                                        else
                                        {
                                            ss.FacilityVacancy = ss.FacilityCapacity;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return s;
                }
            }
            return null;
        }

        /// <summary>
        /// option 11
        /// Calculate SHN Charges
        /// 1. Prompt user for details
        /// </summary>
        /// <param name="pList"></param>
        static void CalculateSHNCharges(List<Person> pList)
        {
            while (true)
            {
                try
                {
                    // prompt user for name
                    Console.Write("Please enter your name: ");
                    string name = Console.ReadLine();

                    // search for the person
                    Person p = SearchPerson(pList, name);
                    if (pList.Contains(p) == true)
                    {
                        // retrieve TravelEntry with SHN ended and is unpaid
                        //DateTime currentTime = DateTime.Now;
                        foreach (TravelEntry te in p.TravelEntryList)
                        {
                            if (te.Ispaid == false)
                            {
                                // call CalculateSHNCharges() to calculate the charges based on the criteria provided in the background brief
                                // if visitor stayed at SDF, call CalculateTravelCost(string,DateTime) to calculate transportation fare. [the input parameters are entryMode and entryDate]
                                // Note: To add 7% GST
                                p.CalculateSHNCharges();

                                Console.WriteLine("Your SHN Charges is ${0:#,##0.00}", p.CalculateSHNCharges());

                                // prompt to make payment
                                Console.WriteLine("Please make your payment!");

                                // change the isPaid Boolean value

                                foreach (TravelEntry t in p.TravelEntryList)
                                {
                                    t.Ispaid = true;
                                }
                            }
                            else
                            {
                                Console.WriteLine("You do not have any appending SHN payment.");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("This person does not exist.");   //validation
                    }
                    break;
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }


        // Advanced Features

        //method to generate tracing report (option12)
        static void TraceReport(List<Person> pList,List<Person> tList)
        {
            while (true)
            {
                try
                {
                    Console.Write("Enter Datetime(yyyy/mm/dd): ");  //prompy user for datetime
                    DateTime traceDate = Convert.ToDateTime(Console.ReadLine());

                    Console.Write("Enter Business Name: "); //prompt user for business name
                    string businessName = Console.ReadLine();

                    foreach (Person p in pList)
                    {
                        foreach (SafeEntry se in p.SafeEntryList)
                        {
                            if (se.Location.BusinessName == businessName && se.CheckIn.ToString("MM/dd/yyyy") == traceDate.ToString("MM/dd/yyyy"))  //check person
                            {
                                tList.Add(p);   //add to list
                            }
                        }
                    }

                    using (StreamWriter sw = new StreamWriter("ContactTrace.csv", false))   //write details to file
                    {
                        sw.WriteLine("{0},{1},{2},{3}", "Name", "Check In", "Check Out", "Business Location");
                        foreach (Person p in tList)
                        {
                            foreach(SafeEntry se in p.SafeEntryList)
                            {
                                sw.WriteLine("{0},{1},{2},{3}", p.Name, se.CheckIn, se.CheckOut, se.Location.BusinessName);
                            }
                        }
                    }
                    Console.WriteLine("The report is ready.");
                    break;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);   //validation
                }
            }
        }

        //method to generate shn status report (option13)
        static void StatusReport(List<Person> pList,List<Person> sList)
        {
            while (true)
            {
                try
                {
                    Console.Write("Enter Date (yyyy/mm/dd): ");
                    DateTime shndate = Convert.ToDateTime(Console.ReadLine());

                    foreach (Person p in pList)
                    {
                        foreach (TravelEntry te in p.TravelEntryList)
                        {
                            if ((te.SHNEndDate > shndate) && (te.EntryDate < shndate))
                            {
                                sList.Add(p);
                            }
                        }
                    }

                    using (StreamWriter sw = new StreamWriter("SHNStatus.csv", false))
                    {
                        sw.WriteLine("{0},{1},{2},{3}", "Name", "Entry Date", "SHN End Date", "SHN Facility");
                        foreach (Person p in sList)
                        {
                            foreach (TravelEntry te in p.TravelEntryList)
                            {
                                if (te.SHNstay != null)
                                {
                                    sw.WriteLine("{0},{1},{2},{3}", p.Name, te.EntryDate, te.SHNEndDate, te.SHNstay.FacilityName);
                                }
                                else
                                {
                                    sw.WriteLine("{0},{1},{2}", p.Name, te.EntryDate, te.SHNEndDate);
                                }
                            }
                        }
                    }
                    Console.WriteLine("The report is ready.");
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Message : " + ex.Message);
                }
            }
        }
    }
}
