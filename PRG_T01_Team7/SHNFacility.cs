﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class SHNFacility
    {
        // Properties

        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }


        // Constructors

        public SHNFacility() { }
        public SHNFacility(string fn, int fc, double dfac, double dfsc, double dflc)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            DistFromAirCheckpoint = dfac;
            DistFromSeaCheckpoint = dfsc;
            DistFromLandCheckpoint = dflc;
        }


        // Methods


        // CalculateTravelCost
        public double CalculateTravelCost(string em, DateTime ed)
        {
            // what is the difference between transpostation and transportation cost?
            //--dcp-- > distcheckpoint(for all)
            //--et-- > entrytime(from entryDate)
            //-- ed-- > entrydate
            //-- em-- > travelentrymode

            // Find the distance of check point
            double dcp = 0;
            if (em == "Air")
            {
                dcp = DistFromAirCheckpoint;
            }
            else if (em == "Sea")
            {
                dcp = DistFromSeaCheckpoint;
            }
            else if (em == "Land")
            {
                dcp = DistFromLandCheckpoint;
            }

            // Find the date and time 
            string fed = DateTime.Now.ToString("dd/MM/yyyy");
            string aed = ed.ToString("dd/MM/yyyy");

            DateTime et1 = Convert.ToDateTime(aed + " " + "06:00");
            DateTime et2 = Convert.ToDateTime(aed + " " + "09:00");
            DateTime et3 = Convert.ToDateTime(aed + " " + "18:00");
            DateTime et4 = Convert.ToDateTime(aed + " " + "00:00");
            DateTime et5 = Convert.ToDateTime(aed + " " + "23:59");


            // Find Transportation Surcharge

            double totaltravelcost = 0;
            if ((ed >= et4) && (ed < et1))
            {
                totaltravelcost = (50 + dcp * 0.22) * 1.5;

            }
            else if ((ed >= et1) && (ed < et2))
            {
                totaltravelcost = (50 + dcp * 0.22) * 1.25;
            }
            else if ((ed >= et3) && (ed <= et5))
            {
                totaltravelcost = (50 + dcp * 0.22) * 1.25;
            }
            else
            {
                totaltravelcost = (50 + dcp * 0.22);
            }

            return totaltravelcost;
        }


        /// <summary>
        /// To see if there is facility vacancy and change the vacancy number when people are assigned to the facilities.
        /// </summary>
        /// <param name="people"></param> Residents and visitors here
        /// <returns></returns>
        public bool IsAvailable(int people)
        {
            FacilityVacancy = FacilityCapacity;
            if (FacilityCapacity > people)
            {
                FacilityVacancy = FacilityCapacity - people;
                return true;
            }
            else
                return false;
        }


        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\tFacility Capacity: " + FacilityCapacity + "\tFacility Vacancy: " + FacilityVacancy + "\tDistFromAirCheckpoint: " + DistFromAirCheckpoint + "\tDistFromSeaCheckpoint: " + DistFromSeaCheckpoint + "\tDistFromLandCheckpoint: " + DistFromLandCheckpoint;
        }
    }
}
