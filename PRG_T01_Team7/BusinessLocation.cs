﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class BusinessLocation
    {
        //property
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }

        //constructor
        public BusinessLocation() { }
        public BusinessLocation(string businessName, string branchCode, int maximumCapacity)
        {
            BusinessName = businessName;
            BranchCode = branchCode;
            MaximumCapacity = maximumCapacity;
        }

        //method to check business is full
        
        public bool IsFull()
        {
            if (VisitorsNow == MaximumCapacity)
            {
                return true;
            }
            else
                return false;
        }
        

        public override string ToString()
        {
            return "Business Name: " + BusinessName + "\tBranch Code: " + BranchCode + "\tMaximum Capacity: " + MaximumCapacity + "\tVisitors Now: "+VisitorsNow;
        }
    }
}
