﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class TravelEntry
    {
        // Properties

        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime SHNEndDate { get; set; }
        public SHNFacility SHNstay { get; set; }
        public bool Ispaid { get; set; }

        // Constructors

        public TravelEntry() { }

        public TravelEntry(string lcoe, string em, DateTime ed)
        {
            LastCountryOfEmbarkation = lcoe;
            EntryMode = em;
            EntryDate = ed;

        }

        // Methods

        /// <summary>
        /// how to assign? based on what criteria?
        /// </summary>
        /// <param name="shnstay"></param>
        // AssignSHNFacility
        public void AssignSHNFacility(SHNFacility shnstay)
        {
            bool success = false;
            if (success)
            {
                Console.WriteLine(shnstay);
            }
        }

        /// <summary>
        /// Calculate the SHN Duration based on the last country of embarkation
        /// </summary>

        // CalculateSHNDuration
        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation == "Macao SAR")
            {
                SHNEndDate = EntryDate.AddDays(7);
            }
            else if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                SHNEndDate = EntryDate.AddDays(0);
            }
            else
            {
                SHNEndDate = EntryDate.AddDays(14);
            }

        }


        public override string ToString()
        {
            return "LastCountryOfEmbarkation: " + LastCountryOfEmbarkation + "\tEntry Mode: " + EntryMode + "\tEntry Date: " + EntryDate + "\tSHN End Date: " + SHNEndDate + "\tSHN stay: " + SHNstay + "\tIs Paid: " + Ispaid;
        }
    }
}
