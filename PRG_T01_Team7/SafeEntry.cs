﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class SafeEntry
    {
        //property
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }

        //constructor
        public SafeEntry () { }

        
        public SafeEntry(DateTime checkIn,BusinessLocation businessLocation)
        {
            CheckIn = checkIn;
            Location = businessLocation;
        }
        
        //function to perform checkout
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
        }

        //tostring
        public override string ToString()
        {
            return "Check In: " + CheckIn + "\tCheck Out: " + CheckOut + "\tLocation: " + Location +"\n";
        }
    }
}
