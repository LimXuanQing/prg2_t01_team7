﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    abstract class Person
    {
        //properties
        public string Name { get; set; }
        private List<SafeEntry> safeEntryList = new List<SafeEntry>();
        
        public List<SafeEntry> SafeEntryList
        {
            get { return safeEntryList; }
        }
        

        private List<TravelEntry> travelEntryList = new List<TravelEntry>();
        
        public List<TravelEntry> TravelEntryList
        {
            get { return travelEntryList; }
        }
        

        public Person() { }
        public Person(string name)
        {
            Name = name;
        }


        //function to add travel entry
        public void AddTravelEntry(TravelEntry te)
        {
            travelEntryList.Add(te);
        }
        

        //function to add safe entry
        public void AddSafeEntry(SafeEntry se)
        {
            safeEntryList.Add(se);
        }

        //abstract method
        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            string seList = "";
            string teList = "";
            foreach (TravelEntry te in travelEntryList)
            {
                teList += te.ToString();
            }

            foreach (SafeEntry se in safeEntryList)
            {
                seList += se.ToString();
            }
            return "Name: " + Name + "\nTravel Entry:" + teList + "\nSafe Entry: " + seList;
        }


    }
}
