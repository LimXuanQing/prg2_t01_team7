﻿//============================================================
// Student Number : S10207455A, S10204389H
// Student Name : Lim Xuan Qing, Boh Chue Yee Shani
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_T01_Team7
{
    class TraceTogetherToken
    {
        //property
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        //constructor
        public TraceTogetherToken () { }
        public TraceTogetherToken (string serialNo, string collectionLocation, DateTime expiryDate)
        {
            SerialNo = serialNo;
            CollectionLocation = collectionLocation;
            ExpiryDate = expiryDate;
        }

        //method to check resident is eligible for token replacement
        public bool IsEligibleForReplacement()
        {
            if ((DateTime.Now).Subtract(ExpiryDate).Days >= 0 && (DateTime.Now).Subtract(ExpiryDate).Days <= 30)   
            {
                return true;
            }
            else
                return false;
        }

        //method to replace token
        public void ReplaceToken(string serialNo, string collectionLocation)
        {
            SerialNo = serialNo;
            CollectionLocation = collectionLocation;
            ExpiryDate = DateTime.Today.AddMonths(6);
            Console.WriteLine("Replacement success, please collect your token.");
        }
        

        public override string ToString()
        {
            return "Serial No: " + SerialNo + "\tCollection Location: " + CollectionLocation + "\tExpiry Date: " + ExpiryDate;
        }
    }
}
